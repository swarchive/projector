# projector

Shockwave Projector Skeleton Archive


# Contributing

Have a version of Director 4+ that is not currently represented here? Please open an issue on this repo providing a copy.


# Info

Every file in `director` includes the files Director uses to create a projector.

Documentation on how to turn these skeletons into actual projectors is out of scope for this repo.

Archive files are organized into directories as follows:

```
director/${director os}/${director version}/${projector os}/
```


# Issues

If you believe you have found a mistake in the included files, some files are missing, or have a version of Director to contribute, please open an issue on this repo.

Please note the software in this repo is provided as-is, and technical support is not provided.
